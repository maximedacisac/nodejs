function findAll(app, connection) {
  app.get("/users", (req, res) => {
    connection.query("SELECT * FROM users", function (err, result) {
      if (err) throw err;
      console.log(result);
      res.send(result);
    });
  });
}

function findOneById(app, connection) {
  app.get("/users/:id", (req, res) => {
    const id = parseInt(req.params.id);
    connection.query(
      "SELECT * FROM users WHERE id =?",
      id,
      function (err, result) {
        if (err) throw err;
        console.log(result);
        res.send(result);
      }
    );
  });
}

function createUser(app, connection) {
  app.post("/users", (req, res) => {
    const newUser = req.body;
    connection.query(
      "INSERT INTO users (id,firstname,lastname,age) VALUES(?)",
      newUser,
      function (err, result) {
        if (err) throw err;
        console.log(newUser);
        res.send(newUser);
      }
    );
  });
}

function updateUser(app, connection) {
  app.put("/users", (req, res) => {
    const newUser = req.body;
    connection.query(
      "UPDATE users SET id=" +
        newUser.id +
        ",firstname= '" +
        newUser.firstname +
        "',lastname='" +
        newUser.lastname +
        "',age=" +
        newUser.age +
        " WHERE ID = " +
        newUser.id,
      function (err, result) {
        if (err) throw err;
        console.log(newUser);
        res.send(newUser);
      }
    );
  });
}

function deleteUser(app, connection) {
  app.delete("/users/:id", (req, res) => {
    const id = parseInt(req.params.id);
    connection.query(
      "DELETE FROM users WHERE id =?",
      id,
      function (err, result) {
        if (err) throw err;
        console.log("suppression de l'utilisateur");
        res.send("suppression de l'utilisateur");
      }
    );
  });
}
