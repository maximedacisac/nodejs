const https = require("https");
const fs = require("fs");
const express = require("express");
var mysql = require("mysql");

var connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "maxime123",
  database: "apirest",
});

try {
  connection.connect();
} catch (err) {
  console.log(err);
}

connection.query("SELECT 1 + 1 AS solution", function (error, results, fields) {
  if (error) throw error;
  console.log("Connected to the database");
});

const app = express();
app.use(express.json());

// app.set("view engine", "ejs");

const options = {
  key: fs.readFileSync("./certs/key.pem"),
  passphrase: "bonjour",
  cert: fs.readFileSync("./certs/cert.pm"),
};

app.get("/users", (req, res) => {
  connection.query("SELECT * FROM users", function (err, result) {
    if (err) throw err;
    console.log(result);
    res.send(JSON.stringify(result));
  });
});

app.get("/users/:id", (req, res) => {
  const id = parseInt(req.params.id);
  connection.query(
    "SELECT * FROM users WHERE id =?",
    id,
    function (err, result) {
      if (err) throw err;
      console.log(result);
      res.send(result);
    }
  );
});

app.post("/users", (req, res) => {
  const newUser = req.body;
  connection.query(
    "INSERT INTO users (id,firstname,lastname,age) VALUES(?)",
    newUser,
    function (err, result) {
      if (err) throw err;
      console.log(newUser);
      res.send(newUser);
    }
  );
});

app.put("/users", (req, res) => {
  const newUser = req.body;
  connection.query(
    "UPDATE users SET id=" +
      newUser.id +
      ",firstname= '" +
      newUser.firstname +
      "',lastname='" +
      newUser.lastname +
      "',age=" +
      newUser.age +
      " WHERE ID = " +
      newUser.id,
    function (err, result) {
      if (err) throw err;
      console.log(newUser);
      res.send(newUser);
    }
  );
});

app.delete("/users/:id", (req, res) => {
  const id = parseInt(req.params.id);
  connection.query("DELETE FROM users WHERE id =?", id, function (err, result) {
    if (err) throw err;
    console.log("suppression de l'utilisateur");
    res.send("suppression de l'utilisateur");
  });
});

// app.use("/", express.static("./assets"));

// app.get("/site", (req, res) => {
//   // res.send('helloworld');
//   res.render("index");
// });

app.use(function (req, res, next) {
  res.status(404).send("404 NOT FOUND");
});

https.createServer(options, app).listen(8401);

// app.listen(8080);
