const {
  existsSync,
  readFile,
  readFileSync,
  stat,
  statSync,
  exists,
  fstat,
  read,
} = require("fs");

module.exports = routeManage = (uri, res) => {
  // mettre les routes en décomposant
  // un maximum de manière fonctionnel et dynamique

  if (uri != "favicon.ico" && uri != "/favicon.ico") {
    let link = `${__dirname}/../view/mirko/${uri}`;
    console.log(link);

    exists(link, (exist) => {
      if (exist) {
        readFile(link, (err, file) => {
          if (err) {
            console.log("err");
            res.end("ERROR 404");
            return;
          }
          //   console.log(file.toString());
          res.statusCode = 200;
          res.setHeader("Content-Type", "text/html");
          res.end(file.toString());
        });
      } else {
        res.end("ERROR 404");
      }
    });

  //   if (uri == "") {
  //     res.end("Hello");
  //     return;
  //   }

  //   try {
  //     const data = read(file);
  //     res.statusCode = 200;
  //     res.setHeader("Content-Type", "text/html");
  //     res.end(data.toString());
  //   } catch (e) {
  //     res.end("ERROR 404");
  //   }
  // }

  // read = async (file) => {
  //   try {
  //     const data = await readFile(file);
  //     console.log(data.toString());
  //   } catch (error) {
  //     console.log(error);
  //   }
  };
};
