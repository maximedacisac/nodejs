const http = require("http");
const {
  existsSync,
  readFile,
  readFileSync,
  stat,
  statSync,
  exists,
  fstat,
} = require("fs");

// const router = require("./router/router.mjs");

const port = 5500;
const host = "127.0.0.1";

const server = http.createServer((request, result) => {
  let uri = request.url.substring(1);
  let response;
  if (uri != "favicon.ico" && uri != "/favicon.ico") {
    // var fileDir = __dirname + "/view" + uri + ".html";
    // router(uri);
    // console.log(fileDir);

    let name = uri.split("/")[1];
    uri = uri.split("/")[0];

    if (name != "" && name != undefined) {
      response = `Bonjour ${name}`;
    } else {
      if (uri == "name" || uri == "name/") {
        response = "Je n'ai pas votre nom";
      }
      if (uri == "info" || uri == "info/") {
        response = "Contenu ";
        exists(__dirname + "/info.json", (exist) => {
          if (exist) {
            readFile(__dirname + "/info.json", (err, file) => {
              if (err) {
                console.log("err");
                return;
              }
              console.log(file.toString());
            });
          }
        });
      }
    }

    console.log(uri);
  }

  result.statusCode = 200;
  result.setHeader("Content-Type", "text/html");
  result.end(response);
});

server.listen(port, host, () => {
  `serveur running on ${host}:${port}`;
});
