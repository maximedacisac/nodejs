const http  = require("http");
const {
  existsSync,
  readFile,
  readFileSync,
  stat,
  statSync,
  exists,
  fstat,
} = require("fs");

const port = 5500;
const host = "127.0.0.1";
const html = "<p>BONJOUR</p>";

const index = readFile("src/index.html", (err, data) => {
  console.log(err);
});

const server = http.createServer();

server.listen(port);

server.once("listening", (args) => {
  console.log(`server running at localhost:${port}/ ${args}`);
});

server.on("request", (req, res) => {
  const { url, headers, statusCode } = req;

  //verifier si le fichier existe

  //METHODE CALLBACK

  exists(__dirname + "/text.txt", (exist) => {
    console.log(exist);

    if (exist) {
      readFile(__dirname + "/text.txt", (err, file) => {
        if (err) {
          console.log("err");
          return;
        }
        console.log(file.toString());
      });
    }
  });

  res.statusCode = 200;
  res.setHeader("Content-Type", "text/html");
  //res.end(url, headers, statusCode);
  res.end("Bonjour");
});

server.on("error", (e) => {
  console.log("error", e);
});
