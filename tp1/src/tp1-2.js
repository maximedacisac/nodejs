const https = require("https");
const fs = require('fs');

const router = require("./router/router.js");

const port = 5600;
const host = "127.0.0.1";

const options = {
  key: fs.readFileSync('./certs/key.pem'),
  passphrase: 'bonjour',
  cert: fs.readFileSync('./certs/cert.pm')
};

const server = https.createServer(options);

server.listen(port);


server.once("listening", (args) => {
  console.log(`server running at localhost:${port}/ ${args}`);
});

server.on("request", (req, res) => {
  const { url, headers, statusCode } = req;
  console.log(url);

  let uri = url.substring(1);
  router(uri, res);
});

server.on("error", (e) => {
  console.log("error", e);
});
