const http = require("http");
const {
  existsSync,
  readFile,
  readFileSync,
  stat,
  statSync,
  exists,
  fstat,
} = require("fs/promises");

const router = require("./router/router.js");

const port = 5500;
const host = "127.0.0.1";

const html = "<p> BONNJOUR MATHIEU</p>";

const server = http.createServer((request, result) => {
  // PROMISE

  //   stat(file)
  //     .then((stat) => {
  //       console.log(stat);

  //       readFile(file)
  //         .then((file) => {
  //           console.log(file.toString());
  //         })
  //         .catch((error) => {
  //           console.log(error);
  //         });
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });

  //AWAIT

  const uri = request.url.substring();
  if (uri != "favicon.ico" && uri != "/favicon.ico") {
    var fileDir = __dirname + "/view" + uri + ".html";
    router(uri);
    console.log(fileDir);
  }
  fileStat(fileDir);

  result.statusCode = 200;
  result.setHeader("Content-Type", "text/html");
  result.end("display");
});

fileStat = async (file) => {
  try {
    const data = await readFile(file);
    console.log(data.toString());
  } catch (error) {
    console.log(error);
  }
};

server.listen(port, host, () => {
  `serveur running on ${host}:${port}`;
});
