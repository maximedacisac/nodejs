const express = require("express");
const fs = require("fs");
const { Server } = require("ws");

const PORT = 8100;

const HOME = "/index.html";

const server = express()
  .use((req, res) => res.sendFile(HOME, { root: __dirname }))

  .listen(PORT, () => console.log(`Listening on ${PORT}`));

const ws = new Server({ server }); // creer le serveur websocket

// traitement à la connection

ws.on("connection", (socket) => {
  socket.on("close", () => console.log("Client disconnected"));
});

const image1 = fs.readFileSync(__dirname + "/images/np1.Jpg",'base64');
const image2 = fs.readFileSync(__dirname + "/images/np2.jpg", 'base64');
const image3 = fs.readFileSync(__dirname + "/images/np3.jpg", 'base64');

const images = [image1, image2, image3];

let index = 0;

// envoie l'heure aux clients toute les secondes

setInterval(() => {
  ws.clients.forEach((client) => {
    client.send(images[index]);
  });
  index = index + 1;
  if (index > images.length - 1) {
    index = 0;
  }
}, 1000);
