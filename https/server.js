const https = require("https");
const fs = require("fs");
const express = require("express");
const fetch = require("node-fetch");

const app = express();
app.set("view engine", "ejs");

const options = {
  key: fs.readFileSync("./certs/key.pem"),
  passphrase: "bonjour",
  cert: fs.readFileSync("./certs/cert.pm"),
};

let plans = [
  {
    title: "Basic bundle",
    description: "basic bundle for free",
    price: 0,
    features: ["Feature 1", "Feature 2", "Feature 3", "Feature 4"],
  },
  {
    title: "Classic bundle",
    description: "Classic bundle for free",
    price: 75,
    features: ["Feature 1", "Feature 2", "Feature 3", "Feature 4"],
  },
  {
    title: "Pro bundle",
    description: "Pro bundle for free",
    price: 200,
    features: ["Feature 1", "Feature 2", "Feature 3", "Feature 4"],
  },
  {
    title: "Pro bundle",
    description: "Pro bundle for free",
    price: 200,
    features: ["Feature 1", "Feature 2", "Feature 3", "Feature 4"],
  },
];

let menu = [
  { title: "Home", sub: [] },
  { title: "About", sub: [] },
  { title: "Service", sub: [] },
  { title: "Plans", sub: [] },
  {
    title: "Drop",
    sub: [
      { title: "Articles Detail", link: "article.html" },
      { title: "Terms condition", link: "terms.html" },
      { title: "Privacy Policy", link: "privacy.html" },
    ],
  },
  { title: "Contact", sub: [] },
];

app.use("/", express.static("./assets"));

app.get("/site", (req, res) => {
  // res.send('helloworld');
  res.render("index", { plans: plans, menu: menu });
});

app.get("/utilisateur", async (req, res) => {
  process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
  try {
    const result = await fetch("https://localhost:8401/users", {
      insecureHTTPParser: true,
    });
    const listUsers = await result.json();
    console.log(listUsers);

    let data = {
      title: "liste utilisateurs",
      utilisateur: listUsers
    };
    res.render("utilisateur", {users : listUsers});
  } catch (e) {
    console.log(e);
    res.send("error");
  }
});

app.use(function (req, res, next) {
  res.status(404).send("404 NOT FOUND");
});

https.createServer(options, app).listen(8400);

// app.listen(8080);
